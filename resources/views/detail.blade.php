@extends('layouts.admin')

@section('main-content')

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Detail Guest') }}</h1>

    @if (session('success'))
    <div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success border-left-success" role="alert">
            {{ session('status') }}
        </div>
    @endif


    <div class="row">

        <!-- Content Column -->
        <div class="col-lg-8">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form class="user">
                        <div class="form-group">
                            <label>Registration Code</label>
                            <input class="form-control" value="{{ $guest->token->kode_register }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>Email address</label>
                            <input class="form-control" value="{{ $guest['email'] }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" value="{{ $guest['name'] }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>Date of birth</label>
                            <input class="form-control" value="{{ date("d, M Y", strtotime($guest['date_of_birth'])) }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>Gender</label>
                            <input class="form-control" value="{{ $guest['gender'] }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>Favorite design</label>
                            <textarea class="form-control" readonly>{{ $guest['favorite_design'] }}</textarea>
                        </div>

                        <div class="text-md-right">
                            <a class="btn btn-danger" href="/home">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
