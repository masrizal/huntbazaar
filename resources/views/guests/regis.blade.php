@extends('layouts.auth')

@section('main-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">

                        <div class="col-lg">
                            <div class="p-5">

                                @if (!$token['kode_register'])
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">{{ __('Form Register') }}</h1>
                                    </div>

                                    @if ($errors->any())
                                        <div class="alert alert-danger border-left-danger" role="alert">
                                            <ul class="pl-4 my-2">
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    {{-- <div id="app">
                                        <example-component></example-component>
                                    </div> --}}

                                    <div class="text-center">
                                        <span>Expired date :</span>
                                    </div>
                                    <p id="timer"></p>

                                    @if (!$expired)
                                        <form method="POST" action="guest_register/add" class="user">
                                            @csrf
                                            <input type="hidden" name="email" value="{{ $token['email'] }}">
                                            <input type="hidden" name="token" value="{{ $token_inv }}">

                                            <div class="form-group">
                                                <label>Email address</label>
                                                <input class="form-control" value="{{ $token['email'] }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter Name">
                                                @error('name')
                                                    <div class="invalid-feedback">
                                                        {{$message}}
                                                    </div>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label>Date of birth</label>
                                                <input type="date" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth">
                                                @error('date_of_birth')
                                                    <div class="invalid-feedback">
                                                        {{$message}}
                                                    </div>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label>Gender</label>
                                                <select class="form-control @error('gender') is-invalid @enderror" name="gender">
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                </select>
                                                @error('gender')
                                                    <div class="invalid-feedback">
                                                        {{$message}}
                                                    </div>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label>Favorite design</label>
                                                <select class="form-control mul-select @error('favorite_design') is-invalid @enderror" multiple="true" name="favorite_design[]">
                                                    <option value="A Bathing Ape">A Bathing Ape</option>
                                                    <option value="A.L.C">A.L.C</option>
                                                    <option value="A.P.C">A.P.C</option>
                                                    <option value="Louis Vuitton">Louis Vuitton</option>
                                                    <option value="M By Missoni">M By Missoni</option>
                                                    <option value="M2Malletier">M2Malletier</option>
                                                    <option value="MAC">MAC</option>
                                                </select>
                                                @error('favorite_design')
                                                    <div class="invalid-feedback">
                                                        {{$message}}
                                                    </div>
                                                @enderror
                                            </div>

                                            <div class="text-md-right">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </form>
                                    @endif

                                @else
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">{{ __('Registration Code') }}</h1>
                                    </div>
                                    <input class="form-control text-center" value="{{ $token['kode_register'] }}" readonly>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
