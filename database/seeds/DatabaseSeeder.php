<?php

use App\Status;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        User::create([
            'name' => 'Admin',
            'last_name' => 'huntbazaar',
            'email' => 'admin@huntbazaar.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi' //password
        ]);

        Status::create([
            'name' => 'not registered'
        ]);

        Status::create([
            'name' => 'registered'
        ]);
    }
}
