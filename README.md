# Huntbazaar

Build with Laravel framework 8.48.2

## Requirements

- PHP >= 7.3.0

## Installation

- Clone the repo and `cd` into it
- Run `composer install`
- Rename or copy `.env.example` file to `.env`
- Run `php artisan key:generate`
- Set your database credentials in your `.env` file

## Note
- Run `php artisan queue:listen` in terminal if you will send email
