<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $guarded = [
        'id'
    ];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function token()
    {
        return $this->belongsTo(Token::class, 'email', 'email');
    }
}
