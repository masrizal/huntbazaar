<?php

namespace App\Http\Controllers;

use App\User;
use App\Guest;
use App\Token;
use Illuminate\Http\Request;
use App\Jobs\InvitationMailJob;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', [
            'guests' => Guest::with('status')->get()
        ]);
    }

    public function detail(Guest $guest)
    {
        return view('detail', [
            'guest' => $guest
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:guests'
        ]);

        $token = $this->generateRandomString(32);

        Guest::create([
            'email' => $request->email,
            'status_id' => 1
        ]);

        Token::create([
            'email' => $request->email,
            'token' => $token
        ]);

        $details = [
            'title' => 'Invitation from Huntbazaar.com',
            'title2' => 'You are invited',
            'body' => 'Please come to our event, Huntbazaar 2021. Aug 22, 2021',
            'email' => $request->email,
            'link' => config('app.url') . '/guest_register?token=' . $token
        ];

        dispatch(new \App\Jobs\InvitationMailJob($details));

        session()->flash('success', 'Invitation created successfully.');
        return redirect('/home');
    }
}
