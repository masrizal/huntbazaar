<?php

namespace App\Http\Controllers;

use App\Guest;
use App\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GuestController extends Controller
{
    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function index()
    {
        if (request('token')) {
            $date_expired = new \Carbon\Carbon('2021-09-19 00:00:00');
            $date_now = \Carbon\Carbon::now();

            if ($date_now >= $date_expired) {
                $data['expired'] = true;
            } else {
                $data['expired'] = false;
            }

            $data['token_inv'] = request('token');
            $data['token'] = Token::where('token', request('token'))->first();

            if (!$data['token']) {
                return abort(404);
            }

            return view('guests/regis', $data);
        } else {

            return abort(404);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'name' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'favorite_design' => 'required',
        ]);

        $now = \Carbon\Carbon::now();

        Guest::where('email', $request->email)->update(
            [
                'status_id' => 2,
                'name' => $request->name,
                'date_of_birth' => $request->date_of_birth,
                'gender' => $request->gender,
                'favorite_design' => implode(", ", $request->favorite_design),
                'invited_at' => $now
            ]
        );

        Token::where('email', $request->email)->update(
            [
                'kode_register' => $this->generateRandomString(12)
            ]
        );

        $details = [
            'title' => 'Thanks you',
            'title2' => 'Thanks for your participation',
            'body' => 'Thanks for your participation. See you later',
            'email' => $request->email,
            'link' => ''
        ];

        $job = (new \App\Jobs\ThanksEmailJob($details))->delay($now->addSeconds(3600));
        dispatch($job);

        return redirect('/guest_register?token=' . $request->token);
    }
}
